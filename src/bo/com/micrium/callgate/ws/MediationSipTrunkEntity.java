package bo.com.micrium.callgate.ws;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class MediationSipTrunkEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private long id;

	private String route;	
	
	private String account;
	
	private String detail;
	
	private String ttstar;
	
	private Date ttend;

	public MediationSipTrunkEntity() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getTtstar() {
		return ttstar;
	}

	public void setTtstar(String ttstar) {
		this.ttstar = ttstar;
	}

	public Date getTtend() {
		return ttend;
	}

	public void setTtend(Date ttend) {
		this.ttend = ttend;
	}
	

}
