package bo.com.micrium.callgate.ws.clientsample;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.jboss.logging.Logger;

import bo.com.micrium.callgate.ws.CallGateWS;
import bo.com.micrium.callgate.ws.CallGateWS_Service;

public class CallGateWsServiceFactory {

	private static Logger log = Logger.getLogger(CallGateWsServiceFactory.class);
	private static Map<String,CallGateWS> ports=new HashMap<String,CallGateWS>();
	private static int pos=0;
	
	public static CallGateWS getPort(String url,String usuario,String pass,Integer timeout,Integer connectionTimeout){
		pos=(pos+1);
		String purl=url;
		CallGateWS port=ports.get("callgate.url"+pos);
		if (port==null){
			try {
				CallGateWS_Service service=new CallGateWS_Service(new URL(purl));
				port=service.getCallGateWSPort();
				
			} catch (Exception e) {
				log.error("SERVICE ERROR al iniciar puerto" +e);
				return port;
			}
			
			try {
                Map<String, Object> req_ctx = ((javax.xml.ws.BindingProvider) port).getRequestContext();
                req_ctx.put("com.sun.xml.ws.internal.request.timeout", new Integer(timeout));
                req_ctx.put("com.sun.xml.ws.internal.connect.timeout", new Integer(connectionTimeout));

                java.util.Map<String, java.util.List<String>> headers = new java.util.HashMap<String, java.util.List<String>>();
                headers.put("usuario", java.util.Collections.singletonList(usuario));
                headers.put("password", java.util.Collections.singletonList(pass));
                req_ctx.put(javax.xml.ws.handler.MessageContext.HTTP_REQUEST_HEADERS, headers);
				
			} catch (Exception e) {
				// TODO: handle exception
				log.error("ERROR setear variables de timeout "+e);
			}
			ports.put("callgate.url"+pos, port);
			
		}else{
			log.info("LISTAR - usando callgate.url "+pos+" :" +url);
		}
		return port;
	}
	
}
