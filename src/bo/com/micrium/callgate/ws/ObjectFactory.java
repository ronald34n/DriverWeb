
package bo.com.micrium.callgate.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bo.com.micrium.callgate.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SaveMediationSipTrunkResponse_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "saveMediationSipTrunkResponse");
    private final static QName _GetAll_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "getAll");
    private final static QName _SaveMediationSipTrunk_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "saveMediationSipTrunk");
    private final static QName _SQLException_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "SQLException");
    private final static QName _GetAllResponse_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "getAllResponse");
    private final static QName _DisableMediationSipTrunkResponse_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "disableMediationSipTrunkResponse");
    private final static QName _GetMediationSipTrunk_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "getMediationSipTrunk");
    private final static QName _ClassNotFoundException_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "ClassNotFoundException");
    private final static QName _DisableMediationSipTrunk_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "disableMediationSipTrunk");
    private final static QName _InstantiationException_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "InstantiationException");
    private final static QName _IllegalAccessException_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "IllegalAccessException");
    private final static QName _GetMediationSipTrunkResponse_QNAME = new QName("http://ws.callgate.micrium.com.bo/", "getMediationSipTrunkResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bo.com.micrium.callgate.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SaveMediationSipTrunkResponse }
     * 
     */
    public SaveMediationSipTrunkResponse createSaveMediationSipTrunkResponse() {
        return new SaveMediationSipTrunkResponse();
    }

    /**
     * Create an instance of {@link GetAll }
     * 
     */
    public GetAll createGetAll() {
        return new GetAll();
    }

    /**
     * Create an instance of {@link SaveMediationSipTrunk }
     * 
     */
    public SaveMediationSipTrunk createSaveMediationSipTrunk() {
        return new SaveMediationSipTrunk();
    }

    /**
     * Create an instance of {@link SQLException }
     * 
     */
    public SQLException createSQLException() {
        return new SQLException();
    }

    /**
     * Create an instance of {@link GetAllResponse }
     * 
     */
    public GetAllResponse createGetAllResponse() {
        return new GetAllResponse();
    }

    /**
     * Create an instance of {@link DisableMediationSipTrunkResponse }
     * 
     */
    public DisableMediationSipTrunkResponse createDisableMediationSipTrunkResponse() {
        return new DisableMediationSipTrunkResponse();
    }

    /**
     * Create an instance of {@link GetMediationSipTrunk }
     * 
     */
    public GetMediationSipTrunk createGetMediationSipTrunk() {
        return new GetMediationSipTrunk();
    }

    /**
     * Create an instance of {@link ClassNotFoundException }
     * 
     */
    public ClassNotFoundException createClassNotFoundException() {
        return new ClassNotFoundException();
    }

    /**
     * Create an instance of {@link GetMediationSipTrunkResponse }
     * 
     */
    public GetMediationSipTrunkResponse createGetMediationSipTrunkResponse() {
        return new GetMediationSipTrunkResponse();
    }

    /**
     * Create an instance of {@link DisableMediationSipTrunk }
     * 
     */
    public DisableMediationSipTrunk createDisableMediationSipTrunk() {
        return new DisableMediationSipTrunk();
    }

    /**
     * Create an instance of {@link InstantiationException }
     * 
     */
    public InstantiationException createInstantiationException() {
        return new InstantiationException();
    }

    /**
     * Create an instance of {@link IllegalAccessException }
     * 
     */
    public IllegalAccessException createIllegalAccessException() {
        return new IllegalAccessException();
    }

    /**
     * Create an instance of {@link MediationSipTrunk }
     * 
     */
    public MediationSipTrunk createMediationSipTrunk() {
        return new MediationSipTrunk();
    }

    /**
     * Create an instance of {@link ResponseType }
     * 
     */
    public ResponseType createResponseType() {
        return new ResponseType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveMediationSipTrunkResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "saveMediationSipTrunkResponse")
    public JAXBElement<SaveMediationSipTrunkResponse> createSaveMediationSipTrunkResponse(SaveMediationSipTrunkResponse value) {
        return new JAXBElement<SaveMediationSipTrunkResponse>(_SaveMediationSipTrunkResponse_QNAME, SaveMediationSipTrunkResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "getAll")
    public JAXBElement<GetAll> createGetAll(GetAll value) {
        return new JAXBElement<GetAll>(_GetAll_QNAME, GetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveMediationSipTrunk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "saveMediationSipTrunk")
    public JAXBElement<SaveMediationSipTrunk> createSaveMediationSipTrunk(SaveMediationSipTrunk value) {
        return new JAXBElement<SaveMediationSipTrunk>(_SaveMediationSipTrunk_QNAME, SaveMediationSipTrunk.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SQLException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "SQLException")
    public JAXBElement<SQLException> createSQLException(SQLException value) {
        return new JAXBElement<SQLException>(_SQLException_QNAME, SQLException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "getAllResponse")
    public JAXBElement<GetAllResponse> createGetAllResponse(GetAllResponse value) {
        return new JAXBElement<GetAllResponse>(_GetAllResponse_QNAME, GetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisableMediationSipTrunkResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "disableMediationSipTrunkResponse")
    public JAXBElement<DisableMediationSipTrunkResponse> createDisableMediationSipTrunkResponse(DisableMediationSipTrunkResponse value) {
        return new JAXBElement<DisableMediationSipTrunkResponse>(_DisableMediationSipTrunkResponse_QNAME, DisableMediationSipTrunkResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMediationSipTrunk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "getMediationSipTrunk")
    public JAXBElement<GetMediationSipTrunk> createGetMediationSipTrunk(GetMediationSipTrunk value) {
        return new JAXBElement<GetMediationSipTrunk>(_GetMediationSipTrunk_QNAME, GetMediationSipTrunk.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClassNotFoundException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "ClassNotFoundException")
    public JAXBElement<ClassNotFoundException> createClassNotFoundException(ClassNotFoundException value) {
        return new JAXBElement<ClassNotFoundException>(_ClassNotFoundException_QNAME, ClassNotFoundException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisableMediationSipTrunk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "disableMediationSipTrunk")
    public JAXBElement<DisableMediationSipTrunk> createDisableMediationSipTrunk(DisableMediationSipTrunk value) {
        return new JAXBElement<DisableMediationSipTrunk>(_DisableMediationSipTrunk_QNAME, DisableMediationSipTrunk.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstantiationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "InstantiationException")
    public JAXBElement<InstantiationException> createInstantiationException(InstantiationException value) {
        return new JAXBElement<InstantiationException>(_InstantiationException_QNAME, InstantiationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IllegalAccessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "IllegalAccessException")
    public JAXBElement<IllegalAccessException> createIllegalAccessException(IllegalAccessException value) {
        return new JAXBElement<IllegalAccessException>(_IllegalAccessException_QNAME, IllegalAccessException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMediationSipTrunkResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.callgate.micrium.com.bo/", name = "getMediationSipTrunkResponse")
    public JAXBElement<GetMediationSipTrunkResponse> createGetMediationSipTrunkResponse(GetMediationSipTrunkResponse value) {
        return new JAXBElement<GetMediationSipTrunkResponse>(_GetMediationSipTrunkResponse_QNAME, GetMediationSipTrunkResponse.class, null, value);
    }

}
