package micrium.user.ldap;

public enum DescriptorBitacora {
	// Parametro
	FORMULARIO("GESTOR FORMULARIO"),

	// Parametro
	PARAMETRO("GESTOR PARAMETRO"),

	// usuario
	USUARIO("GESTOR USUARIO"),

	// grupo
	GRUPO("GESTOR GRUPO"),

	// rol
	ROL("ROL"),

	// gestion de plan
	GESTION_DE_PLAN("Gestion de plan"),

	// solicitar promocion
	SOLICITAR_PROMOCION("Solicitar promocion"),
	
	// gestion de regalo
	GESTION_DE_REGALO("Gestion de regalo"),
	
	// gestion de prodcuto
	GESTION_DE_PRODUCTO("Gestion de producto"),
	
	USUARIO_LOCAL("GESTOR USUARIO LOCAL"),
	
	//Gestion driver ejecucion comando
	EJECUCION_COMANDO("Ejecucion Comando"),
	//Gestion Driver log de transacciones
	LOG_TRANSACCIONES("Log de transacciones"),
	
	GESTION_RANGO_TELEFONIA("Gestion Rango Telefonía");

	private String formulario;

	DescriptorBitacora(String formulario) {
		this.formulario = formulario;
	}

	public String getFormulario() {
		return formulario;
	}

}
