package micrium.user.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class TestFilter
 */
@WebFilter("/CacheFilter")
public class CacheFilter implements Filter {
	
	private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CacheFilter.class);

	/**
	 * Default constructor.
	 */
	public CacheFilter() {
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		CabeceraHttp.ponerCabeceras(request,response);
		//chain.doFilter(request, response);
		request.setCharacterEncoding("UTF-8");
		
			if(ReferrerHttp.isReferrerAjeno(request)){
				//log.info("ReferrerHttp1:");
				log.info("Se va a redireccionar hacia un error la peticion maligna");
				HttpServletResponse httpResponse = (HttpServletResponse) response;
				httpResponse.sendRedirect(loginFilter.rederingErrorHacker);
				chain.doFilter(request, response);
				return;
			}else{
				if( ReferrerHttp.isPageError(request)){
					
					//log.info("ReferrerHttp2:");
					log.debug("Por tratarse de la pagina de error se cancela todo el resto del analisis");
					chain.doFilter(request, response);
					return;
				}
				//log.info("ReferrerHttp3:");
				chain.doFilter(request, response);
			}
		

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
