package micrium.driver.bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.tigo.utils.SysMessage;

import bo.com.micrium.callgate.ws.MediationSipTrunk;
import bo.com.micrium.callgate.ws.MediationSipTrunkEntity;
import bo.com.micrium.callgate.ws.ResponseType;
import micrium.driver.bussines.CallGateBL;
import micrium.user.controler.ControlPrivilegio;
import micrium.user.sys.IControlPrivilegios;

@ManagedBean
@ViewScoped
public class CallGateBean implements Serializable, IControlPrivilegios {
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(CallGateBean.class);

	ControlPrivilegio controlPrivilegio;

	@Inject
	private CallGateBL callBL;

	@Inject
	private MediationSipTrunk mediationSipTrunk;
	
	@Inject
	private MediationSipTrunkEntity mediationSipTrunkEntity;

	private List<MediationSipTrunk> listMediationSipTrunk;
	
	private List<MediationSipTrunkEntity>listaentity;

	private String selectId;
	private boolean visibleEditNew;
	private boolean edit;
	private String selectItemName;
	private String detailItem;
	
	@PostConstruct
	public void init() {
		try {
			cargarRoles(0);
			this.visibleEditNew = false;
			this.controlPrivilegio = ControlPrivilegio.getInstanceControl();
			this.listMediationSipTrunk = callBL.getAll();
			this.mediationSipTrunk = new MediationSipTrunk();
			listaentity=new ArrayList<MediationSipTrunkEntity>();
			listaentity=parserEntity(listMediationSipTrunk);
		} catch (Exception e) {
			log.error("[init] Fallo en el init.", e);
		}
	}

	public void initNew() {
		edit = false;
		visibleEditNew = true;
		this.mediationSipTrunk = new MediationSipTrunk();
	}

	public void saveMediationSipTrunk() throws Exception {
		try {			
			if (mediationSipTrunk.getRoute() == "") {
				SysMessage.warn("Campo Ruta es requerido");
				this.listMediationSipTrunk = callBL.getAll();
				listaentity=parserEntity(listMediationSipTrunk);
				return;
			}

			if (mediationSipTrunk.getAccount() == "") {
				SysMessage.warn("Campo Cuenta es requerido");
				this.listMediationSipTrunk = callBL.getAll();
				listaentity=parserEntity(listMediationSipTrunk);
				return;
			}
			
			if (mediationSipTrunk.getDetail() == "") {
				SysMessage.warn("Campo detalle es requerido");
				this.listMediationSipTrunk = callBL.getAll();
				listaentity=parserEntity(listMediationSipTrunk);
				return;
			}
			if(!edit){
				ResponseType responseType = callBL.saveMediationSipTrunk(mediationSipTrunk);
				this.listMediationSipTrunk = callBL.getAll();
				listaentity=parserEntity(listMediationSipTrunk);
				visibleEditNew = false;
				if(responseType.isStatus()){
					SysMessage.info("DATOS CORRECTOS", responseType.getResponseMessage());
				}else{
					SysMessage.info("ERROR AL PROCESAR", responseType.getResponseMessage());
				}
				
				log.warn("[registrar] se registro el registro con ruta"+this.mediationSipTrunk.getId());
			}else{
				ResponseType responseType = callBL.updateMediationSipTrunk(mediationSipTrunk);
				this.listMediationSipTrunk = callBL.getAll();
				listaentity=parserEntity(listMediationSipTrunk);
				visibleEditNew = false;
				edit = false;
				
				if(responseType.isStatus()){
					log.warn("[actualizar] el registro con id ="+this.mediationSipTrunk.getId()+" fue editado");
					SysMessage.info("DATOS CORRECTOS", responseType.getResponseMessage());
				}else{
					SysMessage.info("ERROR AL PROCESAR", responseType.getResponseMessage());
					log.warn("[actualizar] el registro con id ="+this.mediationSipTrunk.getId()+" fue editado");
				}
			}
			
		} catch (Exception e) {
			SysMessage.error("Error al procesar los datos");
		}
	}

	public void editItem(MediationSipTrunkEntity mediationEntity) {
		MediationSipTrunk newMediation= new MediationSipTrunk();
		newMediation.setAccount(mediationEntity.getAccount());
		newMediation.setRoute(mediationEntity.getRoute());
		newMediation.setDetail(mediationEntity.getDetail());
		newMediation.setId(mediationEntity.getId());
		
		this.mediationSipTrunk = newMediation;
		if (mediationSipTrunk != null) {
			// controlerBitacora.accion(DescriptorBitacora.ROL, "Se pretende
			// editar el ROl: " + rol.getNombre() + ".");
			visibleEditNew = true;
			selectId = mediationSipTrunk.getId() + "";
			edit = true;
			
		} else {
			log.warn("[editar] No se encontro ningun registro seleccionado.");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}

	public void disableMediationSipTrunk() throws Exception {
		try {
			
			if (detailItem== "") {
				SysMessage.warn("Campo detalle es requerido");
				return;
			}
			detailItem = " | "+detailItem;
			ResponseType responseType = callBL.disableMediationSipTrunk((Long.parseLong(selectId)), detailItem);
			this.listMediationSipTrunk = callBL.getAll();
			listaentity=parserEntity(listMediationSipTrunk);
			if(responseType.isStatus()){
				SysMessage.info("DATOS CORRECTOS", responseType.getResponseMessage());
			}else{
				SysMessage.warn("ERROR AL PROCESAR", responseType.getResponseMessage());
			}
			
		} catch (Exception e) {
			log.warn("[Dar de baja] No se encontro ningun registro seleccionado.");
			SysMessage.warn("No se encontro ningun registro seleccionado.");
		}
	}

	public void selectItem() {
		try {
			String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
					.get("itemId");
			selectItemName = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
					.get("itemName");
			selectId = Idstr;
			detailItem = "";
		} catch (Exception e) {
			log.error("[obtenerRole]  error al obtener Parametros Rol: " + e.getMessage(), e);
		}
	}

	public List<MediationSipTrunk> getListMediationSipTrunk() {
		return listMediationSipTrunk;
	}

	public void setListMediationSipTrunk(List<MediationSipTrunk> listMediationSipTrunk) {
		this.listMediationSipTrunk = listMediationSipTrunk;
	}

	public MediationSipTrunk getMediationSipTrunk() {
		return mediationSipTrunk;
	}

	public void setMediationSipTrunk(MediationSipTrunk mediationSipTrunk) {
		this.mediationSipTrunk = mediationSipTrunk;
	}

	public boolean isVisibleEditNew() {
		return visibleEditNew;
	}

	public void setVisibleEditNew(boolean visibleEditNew) {
		this.visibleEditNew = visibleEditNew;
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	@Override
	public boolean isAuthorized(int idAccion) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getSelectId() {
		return selectId;
	}

	public void setSelectId(String selectId) {
		this.selectId = selectId;
	}

	public String getSelectItemName() {
		return selectItemName;
	}

	public void setSelectItemName(String selectItemName) {
		this.selectItemName = selectItemName;
	}

	public void cargarRoles(long rolEditarId) {
		if (rolEditarId == 1) {
			SysMessage.info("No se permite editar el Rol Administracion");
		} else {
			if (rolEditarId > 0) {
				visibleEditNew = true;
				// this.rolId = rolEditarId;
			}
		}
	}

	public Date getDateParser(XMLGregorianCalendar date ) {
		return date.toGregorianCalendar().getTime();
	}

	public String getDetailItem() {
		return detailItem;
	}

	public void setDetailItem(String detailItem) {
		this.detailItem = detailItem;
	}

	
	private List<MediationSipTrunkEntity> parserEntity(List<MediationSipTrunk> list){
		List<MediationSipTrunkEntity>listaentity=new ArrayList<MediationSipTrunkEntity>();
		for (MediationSipTrunk d :list){
			MediationSipTrunkEntity aux =new MediationSipTrunkEntity();
			aux.setId(d.getId());
			aux.setDetail(d.getDetail());
			aux.setAccount(d.getAccount());
			aux.setRoute(d.getRoute());
			if (d.getTtend()!=null){
				aux.setTtend(d.getTtend().toGregorianCalendar().getTime());
			}
			if (d.getTtstar()!=null){
				Calendar calendar = d.getTtstar().toGregorianCalendar();
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				formatter.setTimeZone(calendar.getTimeZone());
				aux.setTtstar(formatter.format(calendar.getTime()));
				
			}
			listaentity.add(aux);
		}
		
		return listaentity;
	}
	public List<MediationSipTrunkEntity> getListaentity() {
		return listaentity;
	}
	public void setListaentity(List<MediationSipTrunkEntity> listaentity) {
		this.listaentity = listaentity;
	}
}
