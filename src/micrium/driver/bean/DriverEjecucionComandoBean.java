package micrium.driver.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.primefaces.context.RequestContext;

import com.tigo.utils.SysMessage;

import micrium.driver.bussines.DriverEjecucionComandoB;
import micrium.driver.bussines.EjecutorComando;
import micrium.driver.model.DriverEjecucionComando;
import micrium.driver.model.LazyDriverEjecucionComando;
import micrium.eventos_bccs.modelo.Cuenta;
import micrium.user.bean.ControlerBitacora;
import micrium.user.id.ParametroID;
import micrium.user.ldap.DescriptorBitacora;
import micrium.user.sys.P;

@ManagedBean(name="DriverEjecucionComandoBean")
@ViewScoped
public class DriverEjecucionComandoBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static Logger log = Logger.getLogger(DriverEjecucionComandoBean.class);
	
	
	@Inject
	private DriverEjecucionComandoB driverejecucionBL;
	
	@Inject
	private ControlerBitacora controlerBitacora;
	
	public DriverEjecucionComando driverejecucioncomando;
	
	@Inject
	private LazyDriverEjecucionComando listaejecucioncomando;
	//variables capturados del .xhtml
	public Date fechainicio;
	public Date fechafin;
	public String cuenta;
	private List<DriverEjecucionComando> listaseleccionados;
	
	private final String LST_CLRDSG="LST_CLRDSG";
	private final String LST_CLDPREANA="LST_CLDPREANA";
	private final String CUENTA="CUENTA";
	private Boolean visibleModal1;
	private Boolean visibleModal2;
	private Boolean visiblepanelgrid;
	
	private Map<String,String>mensajes;
	
	public DriverEjecucionComandoBean() {
		// TODO Auto-generated constructor stub		
	}
	public List<DriverEjecucionComando> getListaseleccionados() {
		return listaseleccionados;
	}
	public void setListaseleccionados(
			List<DriverEjecucionComando> listaseleccionados) {
		this.listaseleccionados = listaseleccionados;
	}
	@PostConstruct
	public void init(){	
		fechafin=new Date();
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DATE,-30);
		fechainicio=cal.getTime();
		visibleModal1=new Boolean(false);
		visibleModal2=new Boolean(false);
		visiblepanelgrid=new Boolean(false);
		mensajes=new HashMap<String,String>();
		listaseleccionados=new ArrayList<DriverEjecucionComando>();
		driverejecucioncomando=new DriverEjecucionComando();
		listaejecucioncomando.filtros(fechainicio, fechafin, cuenta);
		log.info("Se esta iniciando DriverEjecucionComandoBean.....:");
	}
	///////////metodos get y set 
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public Date getFechafin() {
		return fechafin;
	}
	public void setFechafin(Date fechafin) {
		this.fechafin = fechafin;
	}
	public Date getFechainicio() {
		return fechainicio;
	}
	public void setFechainicio(Date fechainicio) {
		this.fechainicio = fechainicio;
	}
	public DriverEjecucionComando getDriverejecucioncomando() {
		return driverejecucioncomando;
	}
	public void setDriverejecucioncomando(
			DriverEjecucionComando driverejecucioncomando) {
		this.driverejecucioncomando = driverejecucioncomando;
	}
	public Boolean getVisibleModal1() {
		return visibleModal1;
	}
	public void setVisibleModal1(Boolean visibleModal1) {
		this.visibleModal1 = visibleModal1;
	}
	public Boolean getVisibleModal2() {
		return visibleModal2;
	}
	public void setVisibleModal2(Boolean visibleModal2) {
		this.visibleModal2 = visibleModal2;
	}
	/////////metodos implementados
	
	public void buscar(){		 
		listaejecucioncomando.filtros(fechainicio, fechafin, cuenta);
		if (!listaejecucioncomando.validar()){
			String m1=listaejecucioncomando.getMensajeCuenta();			
			if (!m1.isEmpty()){
				SysMessage.warn(m1);
			}			
		}else{
			SysMessage.info("Se busco Cuenta :"+cuenta);
		}	
	}	
			
	public LazyDriverEjecucionComando getListaejecucioncomando() {
		return listaejecucioncomando;
	}	
	
	public void ProcesarMultiple(){	
		if (driverejecucionBL.procesarMultiple(listaseleccionados)){
			SysMessage.info("Se proceso exitosamente");
			controlerBitacora.accion(DescriptorBitacora.EJECUCION_COMANDO, "Se procesaron las siguientes cuentas seleccionadas: "+driverejecucionBL.getListaSeleccionados());
		}else{
			SysMessage.warn("no se pudo procesar");
		}
	}
	
	
	public void ConsultarEstadoCuenta(){
		if(listaseleccionados.isEmpty()){
			visibleModal2=true;
			RequestContext.getCurrentInstance().execute(
					"PF('dlgMensaConsulta').show();");
		}else{
			ConsultarEstadoCuentaSeleccionadoTabla();
		}
	}
	
	public void ConsultarEstadoCuentaMSC(){
		if(cuenta.trim().isEmpty()){
			SysMessage.info("Cuenta no puede estar vacio");
			return;
		}
		String patron=P.getValorParametro(ParametroID.EXPRESION_REGULAR_CUENTA);					
		boolean sw=Pattern.matches(patron, cuenta);
		if (!sw){
			SysMessage.info("Cuenta solo son numeros");
			return ;
		}
		
		
		EjecutorComando ejecutor=new EjecutorComando();		
		try {
			ejecutor.init();
			mensajes= ejecutor.ConsultarEstado(this.cuenta);		
			if (mensajes.isEmpty()){
				SysMessage.info("no se pudo consultar");
				return;
			}			
			visibleModal1=true;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR ejecutorComando "+e);
		}
		visiblepanelgrid=true;
	}
	/////////////////
	private void ConsultarEstadoCuentaSeleccionadoTabla(){
		/*if(listaseleccionados.isEmpty()){
			SysMessage.info("Debe seleccionar uno cuenta para consultar su estado");
			return;
		}*/
		if (listaseleccionados.size()>=2){
			SysMessage.info("Solo debe seleccionar una cuenta a consultar");
			return ;
		}
		EjecutorComando ejecutor=new EjecutorComando();
		driverejecucioncomando=listaseleccionados.get(0);
		try {
			ejecutor.init();
			mensajes= ejecutor.ConsultarEstado(driverejecucioncomando.getCuenta());		
			if (mensajes.isEmpty()){
				SysMessage.info("no se pudo consultar");
				return;
			}			
			visibleModal1=true;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR ejecutorComando "+e);
		}
		visiblepanelgrid=true;
		RequestContext.getCurrentInstance().execute(
				"PF('dlgMensaConsulta').show();");
	}
	
	public void Cancelar(){
		visibleModal1=false;
		visibleModal2=false;
		visiblepanelgrid=false;
		mensajes=new HashMap<String,String>();
	}
	public String numeroCuenta(){
		return mensajes.get(CUENTA);
	}
	public String mensajeCLRDSG(){
		return mensajes.get(LST_CLRDSG);
	}
	public String mensajeCLDPREANA(){
		return mensajes.get(LST_CLDPREANA);
	}
	public String nombre(String tramaBCSS){
		return driverejecucionBL.nombreMSC(tramaBCSS);
	}
	public Boolean getVisiblepanelgrid() {
		return visiblepanelgrid;
	}
	public void setVisiblepanelgrid(Boolean visiblepanelgrid) {
		this.visiblepanelgrid = visiblepanelgrid;
	}
	
	public String TipoPlan(String cuenta){
		return driverejecucionBL.nombreTipoPlan(cuenta);
	}
	public String CortoMotivoFallo(String motivofallo){
		try{
		if (motivofallo.isEmpty()){
			return motivofallo;
		}
		int aux=motivofallo.indexOf("RETCODE = 0");
		int aux2=motivofallo.indexOf("(Number of results");
		return (aux>0 && aux2>0)? motivofallo.substring(0,aux+13)+motivofallo.substring(aux2, motivofallo.length()):motivofallo;
		/*if (aux>0 && aux2>0){
			return motivofallo.substring(0,aux+13)+motivofallo.substring(aux2, motivofallo.length());
		}
			return motivofallo;*/
		}catch (Exception e) {
			return motivofallo;
		}
	}	
	public String retcode(){
		return "RETCODE = 0";
	}
	

}
