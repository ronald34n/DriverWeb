package micrium.driver.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import com.tigo.utils.SysMessage;

import micrium.driver.bussines.DriverRangoTelefoniaBL;
import micrium.driver.model.DriverRangoTelefonia;
import micrium.user.bean.ControlerBitacora;
import micrium.user.ldap.DescriptorBitacora;

@ManagedBean
@ViewScoped
public class DriverRangoTelefoniaBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject 
	private DriverRangoTelefoniaBL BL;
	
	private DriverRangoTelefonia driverRangoTelefonia;
	private List<DriverRangoTelefonia>listaDriverRangoTelefonia;
	
	private boolean Editar;
	private Boolean VisibleNuevoEditar;
	
	private String headerCabeceraPopUp;
	
	private String SelectPlanComercial;
	
	private Boolean visibleCambioEstaod;
	
	@Inject
	private ControlerBitacora controlerBitacora;
	
	@PostConstruct
	private void Init(){
		driverRangoTelefonia=new DriverRangoTelefonia();
		listaDriverRangoTelefonia=new ArrayList<DriverRangoTelefonia>();
		Editar=false;
		SelectPlanComercial=new String("0");
		VisibleNuevoEditar=new Boolean(false);
		driverRangoTelefonia.setEstado("AC");
		listaDriverRangoTelefonia=BL.listar();
		visibleCambioEstaod=false;
	}
	
	public void Nuevo(){
		Editar=false;
		VisibleNuevoEditar=true;
		driverRangoTelefonia=new DriverRangoTelefonia();
		headerCabeceraPopUp="Nuevo Rango Telefonía";
		driverRangoTelefonia.setEstado("AC");
	}
	public void Editar(){
		if (driverRangoTelefonia!=null){
			VisibleNuevoEditar=true;
			Editar=true;
			SelectPlanComercial=driverRangoTelefonia.getPlanComercial();
			headerCabeceraPopUp="Editar Rango Telefonía";
			controlerBitacora.accion(DescriptorBitacora.GESTION_RANGO_TELEFONIA, "Se pretende editar el rango telefonia: " + driverRangoTelefonia.getDescripcion() + ".");
		}else{
			SysMessage.info("Debe seleccionar alguno");
		}
	}
	public void Editar(DriverRangoTelefonia modelo){
		this.driverRangoTelefonia=modelo;
		Editar();
	}
	public void Eliminar(){
		if (driverRangoTelefonia!=null){
			if (driverRangoTelefonia.getEstado().equals("EL")){
				driverRangoTelefonia.setEstado("AC");
			}else{
				driverRangoTelefonia.setEstado("EL");
			}
			BL.Modificar(driverRangoTelefonia);
			controlerBitacora.delete(DescriptorBitacora.GESTION_RANGO_TELEFONIA, driverRangoTelefonia.getId() + "", "Descripcion :"+driverRangoTelefonia.getDescripcion()+" Estado :" +driverRangoTelefonia.getEstado());
			listaDriverRangoTelefonia=BL.listar();
			visibleCambioEstaod=false;
			SysMessage.info("Se realizo el cambio estado");
		}else{SysMessage.info("Debe seleccioar un rango");}
			
	}
	public void CancelarEliminar(){
		Editar=false;
		VisibleNuevoEditar=false;
		driverRangoTelefonia=new DriverRangoTelefonia();		
		driverRangoTelefonia.setEstado("AC");
		visibleCambioEstaod=false;
	}
	public void DatoEliminar(DriverRangoTelefonia modelo){
		driverRangoTelefonia=modelo;
		visibleCambioEstaod=true;
	}
	
	public void Save(){
		try{
		if (SelectPlanComercial.equals("0")){
			SysMessage.info("Debe seleccionar un tipo plan");
			return;
		}
		if(SelectPlanComercial.equals("PLAN_800")){	
			//driverRangoTelefonia.setNombrePlanComercial("PLAN 511 - LINEAS 800");
			if(String.valueOf(driverRangoTelefonia.getRangoInicio()).length()!=9){
				SysMessage.info("Rango Inicio debe tener 9 digitos");
				return;
			}
			if(String.valueOf(driverRangoTelefonia.getRangoFin()).length()!=9){
				SysMessage.info("Rango fin debe tener 9 digitos");
				return;
			}
			
		}else{	
			/*if (SelectPlanComercial.equals("TELEFONIA_FIJA")){
				driverRangoTelefonia.setNombrePlanComercial("PLAN 510 - TELEFONIA FIJA");
			}else{
				driverRangoTelefonia.setNombrePlanComercial("PLAN 509 - SIP NUMERACION MOVIL");
			}*/
			if(String.valueOf(driverRangoTelefonia.getRangoInicio()).length()!=8){
				SysMessage.info("Rango Inicio debe tener 8 digitos");
				return;
			}
			if(String.valueOf(driverRangoTelefonia.getRangoFin()).length()!=8){
				SysMessage.info("Rango fin debe tener 8 digitos");
				return;
			}
			
		}
		if(driverRangoTelefonia.getNombrePlanComercial().trim().isEmpty() || driverRangoTelefonia.getNombrePlanComercial().length()>51){
			SysMessage.info("Nombre Plan comercial no puede ser vacio, ni mayor a 50 caracteres");
			return;
		}
		if (driverRangoTelefonia.getDescripcion().trim().isEmpty() || driverRangoTelefonia.getDescripcion().length()>51){
			SysMessage.info("Descripcion Plan no puede ser vacio, ni mayor a 50 caracteres");
			return;
		}
		if(driverRangoTelefonia.getEstado().trim().isEmpty()){
			SysMessage.info("Estado no puede ser Vacio");
			return;
		}
		if(driverRangoTelefonia.getEstado().trim().length()>3){
			SysMessage.info("Estado no puede ser mayor a 2 Caracteres");
		}
		if(driverRangoTelefonia.getRangoInicio()>driverRangoTelefonia.getRangoFin()){
			SysMessage.info("Rango Inicio no puede ser Mayor o igual a Rango Fin");
			return;
		}
		if (driverRangoTelefonia.getRangoInicio()<=0 || driverRangoTelefonia.getRangoFin()<=0){
			SysMessage.info("Rango Inicio o Rango Fin no pueden ser igual o menores a cero");
			return;
		}
		driverRangoTelefonia.setDescripcion(driverRangoTelefonia.getDescripcion().toUpperCase());
		driverRangoTelefonia.setNombrePlanComercial(driverRangoTelefonia.getNombrePlanComercial().toUpperCase());	
		driverRangoTelefonia.setPlanComercial(SelectPlanComercial.toUpperCase());
		driverRangoTelefonia.setEstado(driverRangoTelefonia.getEstado().toUpperCase());
		
		if(Editar){
			if (BL.Modificar(driverRangoTelefonia)){
				controlerBitacora.update(DescriptorBitacora.GESTION_RANGO_TELEFONIA, driverRangoTelefonia.getId() + "", driverRangoTelefonia.getDescripcion());
				SysMessage.info("Se modifico correctamente el Rango Telefonia");
				Editar=false;
				VisibleNuevoEditar=false;
				listaDriverRangoTelefonia=BL.listar();
			}else{
				SysMessage.info("No se pudo guardar");
			}

		}else{
			if (BL.Insertar(driverRangoTelefonia)){
				controlerBitacora.insert(DescriptorBitacora.GESTION_RANGO_TELEFONIA, driverRangoTelefonia.getId() + "", driverRangoTelefonia.getDescripcion());
				SysMessage.info("Se guardo correctamente el Rango Telefonia");
				Editar=false;
				VisibleNuevoEditar=false;
				listaDriverRangoTelefonia=BL.listar();
			}else{
				SysMessage.info("no se pudo guardar");
			}

		}
		
		}catch (Exception e) {
			SysMessage.warn("Fallo al modificar Rango Telefonia");
			
		}
	}
	public void Cancelar(){
		Editar=false;
		VisibleNuevoEditar=false;
		driverRangoTelefonia=new DriverRangoTelefonia();		
		driverRangoTelefonia.setEstado("AC");
	}
	
	public DriverRangoTelefonia getDriverRangoTelefonia() {
		return driverRangoTelefonia;
	}
	public void setDriverRangoTelefonia(DriverRangoTelefonia driverRangoTelefonia) {
		this.driverRangoTelefonia = driverRangoTelefonia;
	}
	public List<DriverRangoTelefonia> getListaDriverRangoTelefonia() {
		return listaDriverRangoTelefonia;
	}
	public void setListaDriverRangoTelefonia(List<DriverRangoTelefonia> listaDriverRangoTelefonia) {
		this.listaDriverRangoTelefonia = listaDriverRangoTelefonia;
	}
	public Boolean getVisibleNuevoEditar() {
		return VisibleNuevoEditar;
	}
	public void setVisibleNuevoEditar(Boolean visibleNuevoEditar) {
		VisibleNuevoEditar = visibleNuevoEditar;
	}
	public String getSelectPlanComercial() {
		return SelectPlanComercial;
	}
	public void setSelectPlanComercial(String selectPlanComercial) {
		SelectPlanComercial = selectPlanComercial;
	}
	public String getHeaderCabeceraPopUp() {
		return headerCabeceraPopUp;
	}
	public void setHeaderCabeceraPopUp(String headerCabeceraPopUp) {
		this.headerCabeceraPopUp = headerCabeceraPopUp;
	}
	public Boolean getVisibleCambioEstaod() {
		return visibleCambioEstaod;
	}
	public void setVisibleCambioEstaod(Boolean visibleCambioEstaod) {
		this.visibleCambioEstaod = visibleCambioEstaod;
	}
}
