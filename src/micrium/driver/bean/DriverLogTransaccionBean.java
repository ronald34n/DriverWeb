package micrium.driver.bean;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.jboss.logging.Logger;
import com.tigo.utils.SysMessage;
import micrium.driver.model.DriverLogTransaccion;
import micrium.driver.model.LazyDriverLogTransaccion;

@ManagedBean(name="driverTransaccionBean")
@ViewScoped
public class DriverLogTransaccionBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(DriverLogTransaccionBean.class);
		
	@Inject
	private LazyDriverLogTransaccion listadriver;
	
	private DriverLogTransaccion driverlogtransaccion;
	
	// variables capturados de xhtml
	private Date fechainicio;
	private Date fechafin;
	private String cuenta;
	
	public DriverLogTransaccionBean() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void init(){
		fechafin=new Date();
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DATE,-30);
		fechainicio=cal.getTime();
		log.info("Iniciando DriverLogTransaccionBean ........");
		listadriver.filtros(fechainicio, fechafin, cuenta);
	}
	
	public DriverLogTransaccion getDriverlogtransaccion() {
		return driverlogtransaccion;
	}
	public void setDriverlogtransaccion(
			DriverLogTransaccion driverlogtransaccion) {
		this.driverlogtransaccion = driverlogtransaccion;
	}
	public Date getFechafin() {
		return fechafin;
	}
	public void setFechafin(Date fechafin) {
		this.fechafin = fechafin;
	}
	public Date getFechainicio() {
		return fechainicio;
	}
	public void setFechainicio(Date fechainicio) {
		this.fechainicio = fechainicio;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public void buscar(){
		listadriver.filtros(fechainicio, fechafin, cuenta);
		if (!listadriver.validar()){
			String m1=listadriver.getMensajeCuenta();
			if (!m1.isEmpty()){
				SysMessage.warn(m1);
			}			
		}else{
			SysMessage.info("Se busco Cuenta :"+cuenta);
		}		
	}
	public LazyDriverLogTransaccion getListadriver() {
		return listadriver;
	}
	public void setListadriver(LazyDriverLogTransaccion listadriver) {
		this.listadriver = listadriver;
	}
}
