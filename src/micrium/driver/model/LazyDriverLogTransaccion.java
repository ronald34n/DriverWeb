package micrium.driver.model;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;

import micrium.driver.bussines.DriverLogTransaccionB;
import micrium.user.id.ParametroID;
import micrium.user.sys.P;

import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.tigo.dao.PQuery;

@Named
public class LazyDriverLogTransaccion extends LazyDataModel<DriverLogTransaccion>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(LazyDriverLogTransaccion.class);
	
	@Inject
	private DriverLogTransaccionB driverlogtransaccionbusiness;
	
	private Integer cantidadTuplas=-1;
	private static String consultaTabla;
	private static PQuery parametrosFiltro;
	
	//variables de filtro
	private Date FechaInicio;
	private Date FechaFin;
	private String Cuenta;
	
	public LazyDriverLogTransaccion() {
		// TODO Auto-generated constructor stub
	}
	
	public void filtros(Date fechainicio,Date fechafin,String cuenta){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechainicio);
		calendar.set(Calendar.HOUR, 01);
		calendar.set(Calendar.MINUTE,00);
		calendar.set(Calendar.SECOND,00);
		FechaInicio=calendar.getTime();
		Calendar cal2=Calendar.getInstance();
		cal2.setTime(fechafin);
		cal2.set(Calendar.HOUR, 23);
		cal2.set(Calendar.MINUTE, 50);
		cal2.set(Calendar.SECOND, 50);
		FechaFin=cal2.getTime();
		Cuenta=cuenta;
	}
	
	@Override
	public Object getRowKey(DriverLogTransaccion object) {
		return object.getId();
	}
	
	@Override
	public DriverLogTransaccion getRowData(String rowKey) {
		try {
			long key = Long.valueOf(rowKey);
			return (DriverLogTransaccion) driverlogtransaccionbusiness.find(key,DriverLogTransaccion.class);
		} catch (Exception e) {
		}
		return null;
	}
	
	
	@Override
	public int getRowCount() {
		if( consultaTabla==null || consultaTabla.isEmpty()){
			String sql = "SELECT COUNT(0) AS TOTAL FROM DRIVER_LOG_TRANSACCION";
			try {
				log.info("CONSULTA NATIVE: "+sql	);
				Object objeto =driverlogtransaccionbusiness.findSingleResultNativeQuery(sql, null);
				if(objeto!=null){
					String numeroEnCadena=String.valueOf(objeto);
					cantidadTuplas=new BigInteger(numeroEnCadena).intValue();
				}else{cantidadTuplas=0;}
			} catch (Exception e) {
				log.error("Fallo al cargar la cantidad de datos existente en el Log Transaccion", e);
			}
		}else{
			if(cantidadTuplas==-1){
				try{
					String total=String.valueOf( driverlogtransaccionbusiness.findSingleResultQuery( consultaTabla.replaceFirst("SELECT b ", "Select count(b) "), parametrosFiltro));
					cantidadTuplas =Integer.parseInt(total);
				}catch(Exception e ){
					log.error("Al contar las tuplas",e);
				}
			}
		}	
		return cantidadTuplas;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DriverLogTransaccion> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT b FROM DriverLogTransaccion b");

			PQuery p = PQuery.getInstancia();
			StringBuilder where = new StringBuilder();
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				if (!entry.getKey().toString().trim().isEmpty()) {
					p.put(entry.getKey(), "%" + entry.getValue().toString().trim().toLowerCase() + "%");// Se ha agregado .trim().toLowerCase()
					if (!entry.getKey().equals("fecha")) {
						where.append("LOWER(b.");
						where.append(entry.getKey());
					} else {
						where.append("TO_CHAR(b.");
						where.append(entry.getKey());
						where.append(", 'DD/MM/YYYY HH24:MI:SS'");
					}
					where.append(") LIKE :");
					where.append(entry.getKey());
					where.append(" AND "); // asume que siempre hay una consulta por delante, despues lo borra mas abajo					
				}
			}
			p.put("fechainicio",FechaInicio);
			p.put("fechafin",FechaFin);
			where.append("b.fechaRegistro between :");
			where.append("fechainicio AND :fechafin");
			where.append(" AND ");
			boolean sw=validar();
			if (sw){
				if (Cuenta!=null && ! Cuenta.trim().isEmpty()){
					p.put("cuenta","%" +Cuenta.trim().toLowerCase()+ "%");
					where.append(" b.cuenta");
					where.append(" LIKE :");
					where.append("cuenta");
					where.append(" AND ");
				}				
			}									
			sb.append(" WHERE ");				
			where.setLength(where.length() - 4); //borra en AND que tiene por demas
			sb.append(where.toString());							
			consultaTabla=sb.toString();//Para contar no necesitamos que ordene, postgre lo rechaza el order by cuando se hace un count
			if(sortField != null && !sortField.trim().isEmpty() && sortOrder != null){
				if ( sortOrder.equals(SortOrder.ASCENDING)) {
					sb.append(" ORDER BY b.");					
					sb.append(sortField);
					sb.append(" ASC ");
				} else if ( sortOrder.equals(SortOrder.DESCENDING)) {
					sb.append(" ORDER BY b.");
					sb.append(sortField);
					sb.append(" DESC ");
				}
			}else{
				sb.append(" ORDER BY b.id DESC");
			}
			cantidadTuplas=-1;//Esto para que actualize la cantidad tras cada página
			log.debug("ConsultaTabla:\n"+consultaTabla);
			log.info("CONSULTA HQL : "+sb.toString());
			parametrosFiltro=p;
			List<DriverLogTransaccion> listaResultante=driverlogtransaccionbusiness.findAllQuery(DriverLogTransaccion.class,sb.toString() , p, first, pageSize);
			return listaResultante;
		} catch (Exception e) {
			log.error("[Fallo al guardar los datos de la DriverLogTransaccion]", e);
		}
		return new ArrayList<DriverLogTransaccion>();
	}
	//////////validaciones 	
	private boolean validarcuenta(){
		MensajeCuenta="";
		if (Cuenta!=null && ! Cuenta.trim().isEmpty()){ //valida si es nulo o tiene caracteres en blanco
			if (Cuenta.length() >7 && Cuenta.length()<10){
				//String patron="^[0-9]+([][0-9]*)?$";
				//String patron="[0-9]*";
				String patron=P.getValorParametro(ParametroID.EXPRESION_REGULAR_CUENTA);				
				boolean sw=Pattern.matches(patron, Cuenta);
				if (sw){
					return true;
				}
				MensajeCuenta="Cuenta no valida, solo ingrese valores numéricos";
				return false;
			} //longitud no adecuada tiene que ser 8 u 9
			MensajeCuenta="Longitud de cuenta no valida";
			return false;
		}
		return false;
	}
	
	public boolean validar(){
		if (validarcuenta() ){
			return true;
		}		
		return false;
	}
	private String MensajeCuenta;
		
	public String getMensajeCuenta() {
		return MensajeCuenta;
	}

}
