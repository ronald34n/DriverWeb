package micrium.driver.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DRIVER_LOG_TRANSACCION database table.
 * 
 */
@Entity
@Table(name="DRIVER_LOG_TRANSACCION")
@NamedQuery(name="DriverLogTransaccion.findAll", query="SELECT d FROM DriverLogTransaccion d")
public class DriverLogTransaccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	private String accion;

	private String cuenta;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_REGISTRO")
	private Date fechaRegistro;

	@Id
	@SequenceGenerator(name = "DRIVER_LOG_TRANSACCION_ID_GENERATOR", sequenceName = "DRIVER_SEQ_LOG_TRANSACCION", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DRIVER_LOG_TRANSACCION_ID_GENERATOR")
	private int id;

	private String tipo;

	private String usuario;

	public DriverLogTransaccion() {
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}