package micrium.driver.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="DRIVER_RANGO_TELEFONIA")
@NamedQuery(name="DriverRangoTelefonia.findAll", query="SELECT d FROM DriverRangoTelefonia d")
public class DriverRangoTelefonia implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@SequenceGenerator(name = "DRIVER_RANGO_TELEFONIA_ID_GENERATOR", sequenceName = "DRIVER_SEQ_RANGO_TELEFONIA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DRIVER_RANGO_TELEFONIA_ID_GENERATOR")
	private int id;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="RANGO_INICIO")
	private int rangoInicio;

	@Column(name="RANGO_FIN")
	private int rangoFin;
	
	private String estado;
	
	@Column(name="PLAN_COMERCIAL")
	private String planComercial;
	
	@Column(name="NOMBRE_PLAN_COMERCIAL")
	private String nombrePlanComercial;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getRangoInicio() {
		return rangoInicio;
	}

	public void setRangoInicio(int rangoInicio) {
		this.rangoInicio = rangoInicio;
	}

	public int getRangoFin() {
		return rangoFin;
	}

	public void setRangoFin(int rangoFin) {
		this.rangoFin = rangoFin;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPlanComercial() {
		return planComercial;
	}

	public void setPlanComercial(String planComercial) {
		this.planComercial = planComercial;
	}
	public String getNombrePlanComercial() {
		return nombrePlanComercial;
	}
	public void setNombrePlanComercial(String nombrePlanComercial) {
		this.nombrePlanComercial = nombrePlanComercial;
	}
	
}