package micrium.driver.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the DRIVER_COLA_TRAMA database table.
 * 
 */
@Entity
@Table(name="DRIVER_COLA_TRAMA")
@NamedQuery(name="DriverColaTrama.findAll", query="SELECT d FROM DriverColaTrama d")
public class DriverColaTrama implements Serializable {
	private static final long serialVersionUID = 1L;

	private String cuenta;
	
	@Id
	@SequenceGenerator(name = "DRIVER_COLA_TRAMA_ID_GENERATOR", sequenceName = "DRIVER_SEQ_COLA_TRAMA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DRIVER_COLA_TRAMA_ID_GENERATOR")
	private int id;

	@Column(name="TRAMA_BCCS")
	private String tramaBccs;

	private String usuario;

	public DriverColaTrama() {
	}

	public String getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTramaBccs() {
		return this.tramaBccs;
	}

	public void setTramaBccs(String tramaBccs) {
		this.tramaBccs = tramaBccs;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}