package micrium.driver.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the DRIVER_CONFIGURACION_COMANDO database table.
 * 
 */
@Entity
@Table(name="DRIVER_CONFIGURACION_COMANDO")
@NamedQuery(name="DriverConfiguracionComando.findAll", query="SELECT d FROM DriverConfiguracionComando d")
public class DriverConfiguracionComando implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="ESTADO_BCCS")
	private String estadoBccs;

	@Column(name="PLAN_COMERCIAL")
	private String planComercial;

	//bi-directional many-to-one association to DriverComando
	@ManyToOne
	@JoinColumn(name="COMANDO_ID")
	private DriverComando driverComando;

	public DriverConfiguracionComando() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstadoBccs() {
		return this.estadoBccs;
	}

	public void setEstadoBccs(String estadoBccs) {
		this.estadoBccs = estadoBccs;
	}

	public String getPlanComercial() {
		return this.planComercial;
	}

	public void setPlanComercial(String planComercial) {
		this.planComercial = planComercial;
	}

	public DriverComando getDriverComando() {
		return this.driverComando;
	}

	public void setDriverComando(DriverComando driverComando) {
		this.driverComando = driverComando;
	}

}