package micrium.driver.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the DRIVER_COMANDO database table.
 * 
 */
@Entity
@Table(name="DRIVER_COMANDO")
@NamedQuery(name="DriverComando.findAll", query="SELECT d FROM DriverComando d")
public class DriverComando implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String comando;

	private String nombre;

	//bi-directional many-to-one association to DriverConfiguracionComando
	@OneToMany(mappedBy="driverComando")
	private List<DriverConfiguracionComando> driverConfiguracionComandos;

	public DriverComando() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComando() {
		return this.comando;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<DriverConfiguracionComando> getDriverConfiguracionComandos() {
		return this.driverConfiguracionComandos;
	}

	public void setDriverConfiguracionComandos(List<DriverConfiguracionComando> driverConfiguracionComandos) {
		this.driverConfiguracionComandos = driverConfiguracionComandos;
	}

	public DriverConfiguracionComando addDriverConfiguracionComando(DriverConfiguracionComando driverConfiguracionComando) {
		getDriverConfiguracionComandos().add(driverConfiguracionComando);
		driverConfiguracionComando.setDriverComando(this);

		return driverConfiguracionComando;
	}

	public DriverConfiguracionComando removeDriverConfiguracionComando(DriverConfiguracionComando driverConfiguracionComando) {
		getDriverConfiguracionComandos().remove(driverConfiguracionComando);
		driverConfiguracionComando.setDriverComando(null);

		return driverConfiguracionComando;
	}

}