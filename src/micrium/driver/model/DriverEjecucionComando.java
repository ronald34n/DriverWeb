package micrium.driver.model;

import java.io.Serializable;
import javax.persistence.*;
import micrium.user.id.ParametroID;
import micrium.user.sys.P;
import java.util.Date;


/**
 * The persistent class for the DRIVER_EJECUCION_COMANDO database table.
 * 
 */
@Entity
@Table(name="DRIVER_EJECUCION_COMANDO")
@NamedQuery(name="DriverEjecucionComando.findAll", query="SELECT d FROM DriverEjecucionComando d")
public class DriverEjecucionComando implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "DRIVER_EJECUCION_COMANDO_ID_GENERATOR", sequenceName = "DRIVER_SEQ_EJECUCION_COMANDO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DRIVER_EJECUCION_COMANDO_ID_GENERATOR")
	private int id;
	
	@Column(name="COMANDO_MSC")
	private String comandoMsc;

	private String cuenta;

	private String estado;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_EJECUCION")
	private Date fechaEjecucion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_REGISTRO")
	private Date fechaRegistro;

	@Column(name="MOTIVO_FALLO")
	private String motivoFallo;

	@Column(name="TRAMA_BCCS")
	private String tramaBccs;

	private String usuario;

	public DriverEjecucionComando() {
	}

	public String getComandoMsc() {
		return this.comandoMsc;
	}

	public void setComandoMsc(String comandoMsc) {
		this.comandoMsc = comandoMsc;
	}

	public String getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaEjecucion() {
		return this.fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMotivoFallo() {
		return this.motivoFallo;
	}

	public void setMotivoFallo(String motivoFallo) {
		this.motivoFallo = motivoFallo;
	}

	public String getTramaBccs() {
		return this.tramaBccs;
	}

	public void setTramaBccs(String tramaBccs) {
		this.tramaBccs = tramaBccs;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getEstadoBCCS(){
		String separador = (String) P.getParamVal(ParametroID.JMS_SEPARADOR_TRAMA);
		String trama=tramaBccs;
		String[] ss = trama.trim().split(separador);	
		return ss[9];
	}	
	
}