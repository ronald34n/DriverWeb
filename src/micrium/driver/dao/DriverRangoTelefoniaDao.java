package micrium.driver.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import micrium.driver.model.DriverRangoTelefonia;

public class DriverRangoTelefoniaDao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static Logger log = Logger.getLogger(DriverRangoTelefoniaDao.class);
	
	@PersistenceContext
	private transient EntityManager entityManager;

	@Resource
	private transient UserTransaction transaction;

	public boolean Insertar(DriverRangoTelefonia driverrangotelefonia){
		try{
		transaction.begin();
			entityManager.persist(driverrangotelefonia);
		transaction.commit();
		return true;
		}catch (Exception e) {
			log.error("ERRRO insertar Driver Rango Telefonia");
			return false;
		}
		
	}
	public boolean Modificar(DriverRangoTelefonia driverrangotelefonia){
		
		try{
			transaction.begin();	
			entityManager.merge(driverrangotelefonia);
			transaction.commit();
			return true;
		}catch (Exception e) {
			log.error("ERRRO insertar Driver Rango Telefonia");
			return false;
		}
		
			
	}
	
	public List<DriverRangoTelefonia> Listar(){		
		try{
			return entityManager.createQuery("Select d From DriverRangoTelefonia d Order by d.id Desc",DriverRangoTelefonia.class).getResultList();
		}catch (Exception e) {
			log.error("ERROR LISTAR RANGOTELEFONIA"+e);
			return null;
		}
	}
	public String obtenerPlanComercial(int numero) {
		// TODO Auto-generated method stub
		
		String query = "SELECT PLAN_COMERCIAL FROM DRIVER_RANGO_TELEFONIA WHERE ESTADO='AC' AND ? BETWEEN RANGO_INICIO AND RANGO_FIN";
		
		List<DriverRangoTelefonia>list= entityManager.createNamedQuery(query,DriverRangoTelefonia.class).setParameter(0,numero).getResultList();
		if (list!=null){
			return list.get(0).getPlanComercial();
		}
		log.error("Fallo obtener plan comercial");
		return null;
	}
}
