package micrium.driver.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import bo.com.micrium.callgate.ws.CallGateWS;
import bo.com.micrium.callgate.ws.MediationSipTrunk;
import bo.com.micrium.callgate.ws.ResponseType;
import bo.com.micrium.callgate.ws.clientsample.CallGateWsServiceFactory;
import micrium.user.bean.ControlerBitacora;
import micrium.user.id.ParametroID;
import micrium.user.sys.P;

@Named
public class CallGateDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(CallGateDAO.class);
	@Inject
	private ControlerBitacora controlerBitacora;
	
	public List<MediationSipTrunk> getAll() throws Exception {		
		String url = P.getValorParametro(ParametroID.ADDRES_WS_CALLGATE);
		String usuario = "";
		String pass = "";
		log.info("[invocando WebService - CalllGate]: "+ url+" - metodo: getAll()");
		Integer timeout = ((BigDecimal)P.getParamVal(ParametroID.TIMEOUT_WS_CALLGATE)).intValue();
		Integer connectionTimeout = 3;
		CallGateWS callgatews = CallGateWsServiceFactory.getPort(url, usuario, pass, timeout, connectionTimeout);

		ResponseType response = callgatews.getAll();

		return response.getList();
	}

	public MediationSipTrunk getMediationSipTrunk(long id) throws Exception {
		log.info("[invocando WebService - CalllGate]: "+ P.getValorParametro(ParametroID.ADDRES_WS_CALLGATE));
		String url = P.getValorParametro(ParametroID.ADDRES_WS_CALLGATE);
		String usuario = "";
		String pass = "";
		log.info("[invocando WebService - CalllGate]: "+ url+" - metodo: getMediationSipTrunk()");
		Integer timeout = ((BigDecimal)P.getParamVal(ParametroID.TIMEOUT_WS_CALLGATE)).intValue();
		Integer connectionTimeout = 3;
		CallGateWS callgatews = CallGateWsServiceFactory.getPort(url, usuario, pass, timeout, connectionTimeout);

		return null;
	}

	public ResponseType disableMediationSipTrunk(long MediationSipTrunkId, String detail) throws Exception {
		String url = P.getValorParametro(ParametroID.ADDRES_WS_CALLGATE);
		String usuario = "";
		String pass = "";
		log.info("[invocando WebService - CalllGate]: "+ url+" - metodo: disableMediationSipTrunk()");
		Integer timeout = ((BigDecimal)P.getParamVal(ParametroID.TIMEOUT_WS_CALLGATE)).intValue();
		Integer connectionTimeout = 3;
		CallGateWS callgatews = CallGateWsServiceFactory.getPort(url, usuario, pass, timeout, connectionTimeout);
		ResponseType responseType = callgatews.disableMediationSipTrunk(MediationSipTrunkId,detail);
		return responseType;
	}

	public ResponseType saveMediationSipTrunk(MediationSipTrunk mediationSipTrunk) throws Exception {
		String url = P.getValorParametro(ParametroID.ADDRES_WS_CALLGATE);
		String usuario = "";
		String pass = "";
		log.info("[invocando WebService - CalllGate]: "+ url+" - metodo: saveMediationSipTrunk()");
		Integer timeout = ((BigDecimal)P.getParamVal(ParametroID.TIMEOUT_WS_CALLGATE)).intValue();
		Integer connectionTimeout = 3;
		CallGateWS callgatews = CallGateWsServiceFactory.getPort(url, usuario, pass, timeout, connectionTimeout);
		ResponseType responseType = callgatews.saveMediationSipTrunk(mediationSipTrunk.getRoute(),
				mediationSipTrunk.getAccount(), mediationSipTrunk.getDetail());
		
		return responseType;
	}
	
	public ResponseType updateMediationSipTrunk(MediationSipTrunk mediationSipTrunk) throws Exception {
		String url = P.getValorParametro(ParametroID.ADDRES_WS_CALLGATE);
		String usuario = "";
		String pass = "";
		log.info("[invocando WebService - CalllGate]: "+ url+" - metodo: saveMediationSipTrunk()");
		Integer timeout = ((BigDecimal)P.getParamVal(ParametroID.TIMEOUT_WS_CALLGATE)).intValue();
		Integer connectionTimeout = 3;
		CallGateWS callgatews = CallGateWsServiceFactory.getPort(url, usuario, pass, timeout, connectionTimeout);
		ResponseType responseType = callgatews.updateMediationSipTrunk(mediationSipTrunk.getId(), mediationSipTrunk.getRoute(), mediationSipTrunk.getAccount(), mediationSipTrunk.getDetail());
		return responseType;
	}
}
