package micrium.driver.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import micrium.driver.model.DriverColaTrama;
import micrium.driver.model.DriverComando;
import micrium.driver.model.DriverEjecucionComando;
import micrium.driver.model.DriverRangoTelefonia;
import micrium.user.id.ParametroID;
import micrium.user.sys.P;
@Named
public class DriverEjecucionComandoDao implements Serializable{
	
public static Logger log = Logger.getLogger(DriverEjecucionComandoDao.class);

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private transient EntityManager entityManager;

	@Resource
	private transient UserTransaction transaction;
	
	@SuppressWarnings("unchecked")
	public List<DriverEjecucionComando> listar(){
		try {
			Query query=entityManager.createQuery("SELECT d FROM DriverEjecucionComando d");
			return query.getResultList();
		} catch (Exception e) {
			List<DriverEjecucionComando> d=new ArrayList<DriverEjecucionComando>();
			log.info("ERROR: DriverEjecucionComandoDao:lista "+e);
			return d;
		}
	}
	
	public void actualizar(DriverEjecucionComando d,DriverColaTrama dct){
		try {
			transaction.begin();
			entityManager.persist(dct);
			entityManager.merge(d);
			transaction.commit();
		} catch (Exception e) {			
			log.info("ERROR: no se pudo actualizar DriverEjecucionComando su estado "+e);			
		}
	}
	
	public boolean ActualizarEstados(List<DriverEjecucionComando> lista){
		try {
			transaction.begin();
			for (DriverEjecucionComando Dec:lista){
				if (Dec.getEstado().equals("ERROR")){
					Dec.setEstado("PROCESANDO");
					entityManager.merge(Dec);
				}					
			}	
			transaction.commit();
			log.info("Se actualizo correctamente los estados");
			return true;
		} catch (Exception e) {
			log.error("ERRROR actualizar estados");
			return false;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public String nombreMSC(String tramaBCSS){
		String separador = (String) P.getParamVal(ParametroID.JMS_SEPARADOR_TRAMA);
		String[] ss = tramaBCSS.trim().split(separador);
		String ac=ss[9];
		String cuenta=ss[0];
		if ((cuenta==null) || (cuenta.equals("null"))){
			log.error("ERROR cuenta null nombreMSC");
			return " ";
		}
		String tipotelefonia= gettipotelefonia(cuenta);
		log.info("TipoPlan: "+tipotelefonia);
		try{
			StringBuffer sql=new StringBuffer(64);
			sql.append("SELECT b FROM DriverConfiguracionComando a, DriverComando b "
					+ " WHERE a.driverComando.id = b.id AND a.estadoBccs =:ac AND a.planComercial =:tipotelefonia");			
			Query query=entityManager.createQuery(sql.toString(),DriverComando.class);
			query.setParameter("ac",ac );
			query.setParameter("tipotelefonia", tipotelefonia);
			List<DriverComando>list=query.getResultList();
			if (list==null){
				log.info("Lista null NombreMSC");
				return "";
			}
			StringBuffer nombremsc=new StringBuffer(64);
			for (DriverComando d:list){
				//nombremsc=nombremsc.append(d.getNombre()+",");
				nombremsc.append(d.getNombre()+" Y ");
			}
			nombremsc.setLength(nombremsc.length()-2);
			return nombremsc.toString();
		}catch(Exception e)
		{
			log.error("ERROR NOMBRE MSC"+e);
			return "";
		}		
	}
	
	@SuppressWarnings("unchecked")
	public String gettipotelefonia(String cuenta){
		int numcuenta=Integer.parseInt(cuenta);
		String sql="SELECT RT FROM DriverRangoTelefonia RT WHERE RT.estado='AC' AND :numero BETWEEN RT.rangoInicio AND RT.rangoFin";
		log.info("Ejecutanto query para obtenr plan comercial"+sql);
		Query query=entityManager.createQuery(sql.toString(),DriverRangoTelefonia.class);
		query.setParameter("numero",numcuenta);
		List<DriverRangoTelefonia> list=query.getResultList();
		if(list==null|| list.isEmpty()){
			return "";
		}
		return list.get(0).getPlanComercial();
		
//		int initFija = ((BigDecimal) P.getParamVal(ParametroID.RANGO_INICIO_TELEFONIA_FIJA)).intValue();
//		//int fija =( (BigDecimal) P.getParamVal(ParametroID.RANGO_INICIO_TELEFONIA_FIJA)).intValue();
//		int finFija = ((BigDecimal) P.getParamVal(ParametroID.RANGO_FIN_TELEFONIA_FIJA)).intValue();
//		if (numcuenta >= initFija && numcuenta <= finFija) {
//		return Tipotelefonia.TELEFONIA_FIJA;
//		}
//
//		int initPlan800 = ((BigDecimal) P.getParamVal(ParametroID.RANGO_INICIO_PLAN_800)).intValue();
//		int finPlan800 = ((BigDecimal) P.getParamVal(ParametroID.RANGO_FIN_PLAN_800)).intValue();
//		if (numcuenta >= initPlan800 && numcuenta <= finPlan800) 
//		{
//			return Tipotelefonia.PLAN_800;			
//		}else{
//			return Tipotelefonia.COMANDO_LISTA;
//		}
//		
		
				
	}

	
}