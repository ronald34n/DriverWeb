package micrium.driver.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import micrium.driver.model.DriverColaTrama;
import micrium.driver.model.DriverEjecucionComando;

import org.apache.log4j.Logger;

public class DriverColaTramaDao implements Serializable{
public static Logger log = Logger.getLogger(DriverColaTramaDao.class);
	
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private transient EntityManager entityManager;

	@Resource
	private transient UserTransaction transaction;

	public void insertar(DriverColaTrama d){
		try{
			transaction.begin();
			entityManager.persist(d);
			transaction.commit();
		}catch(Exception e){
			log.info("Error: DriverColaTramaDao: insertar:"+e);
		}
	}
	
	public boolean procesarMultiple(List<DriverColaTrama> listatrama,List<DriverEjecucionComando> listasEjecucionComando){		
		try{
				transaction.begin();
				for (DriverColaTrama dct:listatrama){
					entityManager.persist(dct);
				}
				for (DriverEjecucionComando Dec:listasEjecucionComando){
					if (Dec.getEstado().equals("ERROR")){
						Dec.setEstado("PROCESANDO");
						entityManager.merge(Dec);
					}					
				}	
			transaction.commit();
			log.info("Inserto Exitosamente los seleccionados DriverColaTrama");
			return true;
		}catch(Exception e){
			try {
				transaction.rollback();
			} catch (IllegalStateException | SecurityException
					| SystemException e1) {
				e1.printStackTrace();
				log.error("ERRO rollback");
			}
			log.error("ERROR procesarMultiple :"+e);
			return false;
		}
	}
}
