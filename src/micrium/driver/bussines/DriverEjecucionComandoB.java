package micrium.driver.bussines;

import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSException;
import javax.naming.NamingException;
import micrium.driver.dao.DriverEjecucionComandoDao;
import micrium.driver.model.DriverEjecucionComando;

import org.jboss.logging.Logger;

import com.tigo.dao.MasterDao;

@Named
public class DriverEjecucionComandoB extends MasterDao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static Logger log = Logger.getLogger(DriverEjecucionComandoB.class);

	@Inject
	private DriverEjecucionComandoDao ejecucioncomandodao;

		
	public boolean procesarMultiple(List<DriverEjecucionComando> SeleccionadosEjecucionComando){
		if (SeleccionadosEjecucionComando.isEmpty()){
			return false;
		}		
		for (DriverEjecucionComando Dec:SeleccionadosEjecucionComando){
			if (!Dec.getEstado().equals("ERROR")){
				return false; //Si selecciono alguno distinto de ERROR entonces devuelve falso, solo
			}			
		}			
		
		try {
			StringBuffer seleccionados=new StringBuffer();
			for (DriverEjecucionComando d:SeleccionadosEjecucionComando){
				seleccionados.append(d.getCuenta()+",");
				ProductorTramasCola pt=new ProductorTramasCola();
				pt.abrirconexion();
				pt.sendCuenta(d);
				pt.close();
				
			}				
			ListaSeleccionados=seleccionados.toString();
			return ejecucioncomandodao.ActualizarEstados(SeleccionadosEjecucionComando);
		} catch (NamingException e) {
			log.error("ERROR conexion1 : ",e);
			e.printStackTrace();
			return false;
		} catch (JMSException e) {
			log.error("ERROR conexion2 : ",e);
			e.printStackTrace();
		}
		return false;
	}
		
	
	private String ListaSeleccionados;
	public String getListaSeleccionados() {
		return ListaSeleccionados;
	}
	
	public String nombreMSC(String tramaBCSS){
		return ejecucioncomandodao.nombreMSC(tramaBCSS);
	}

	public String nombreTipoPlan(String cuenta) {
		// TODO Auto-generated method stub
		return ejecucioncomandodao.gettipotelefonia(cuenta);
	}
	
}
