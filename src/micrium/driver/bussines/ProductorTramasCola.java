/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.driver.bussines;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import micrium.driver.model.DriverEjecucionComando;
import micrium.eventos_bccs.modelo.Cuenta;
import micrium.user.id.ParametroID;
import micrium.user.sys.P;

import org.jboss.logging.Logger;

/**
 *
 * @author SoftWill
 */
public class ProductorTramasCola {
	//variables para la conexion
	private ObjectMessage objectMessage;
	private QueueConnectionFactory queueConnectionFactory;
	private QueueConnection queueConnection;
	private QueueSession queueSession;
	private Session session;
	private Queue queue;
	private QueueSender queueSender;
	//otros
	private List<DriverEjecucionComando> listadriverejecucion;
	private String cuentaenviadas;

	public static Logger log = Logger.getLogger(ProductorTramasCola.class);

	public ProductorTramasCola() throws NamingException, JMSException {
		cuentaenviadas="";
		listadriverejecucion=new ArrayList<DriverEjecucionComando>();
	}

	private void init(Context ctx, String queueName) throws NamingException,
			JMSException {
		String jmsRemoteConnectionFactory = (String) P
				.getParamVal(micrium.user.id.ParametroID.JMS_REMOTE_CONNECTION_FACTORY);
		String jbossAplicationUser = (String) P
				.getParamVal(micrium.user.id.ParametroID.JBOSS_APPLICATION_USER);
		String jbossAplicationPassword = (String) P
				.getParamVal(ParametroID.JBOSS_APPLICATION_PASSWORD);
		queueConnectionFactory = (QueueConnectionFactory) ctx
				.lookup(jmsRemoteConnectionFactory);
		queueConnection = queueConnectionFactory.createQueueConnection(
				jbossAplicationUser, jbossAplicationPassword);

		session = queueConnection.createQueueSession(false,
				Session.AUTO_ACKNOWLEDGE);
		queueSession = queueConnection.createQueueSession(false,
				Session.AUTO_ACKNOWLEDGE);
		queue = (Queue) ctx.lookup(queueName);

		queueSender = queueSession.createSender(queue);
		objectMessage = session.createObjectMessage();
		queueConnection.start();
	}

	private static InitialContext getInitialContext() throws NamingException {
		String jbossAplicationUser = (String) P
				.getParamVal(ParametroID.JBOSS_APPLICATION_USER);
		String jbossAplicationPassword = (String) P
				.getParamVal(ParametroID.JBOSS_APPLICATION_PASSWORD);
		String remoteUrl = (String) P.getParamVal(ParametroID.REMOTE_URL);
		 String initialContextFactory = (String) P.getParamVal(ParametroID.INITIAL_CONTEXT_FACTORY);
		Properties p = new Properties();
		p.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
		p.put(Context.PROVIDER_URL, remoteUrl);
		p.put(Context.SECURITY_PRINCIPAL, jbossAplicationUser);
		p.put(Context.SECURITY_CREDENTIALS, jbossAplicationPassword);
		return new InitialContext(p);
	}
	public void sendCuenta(DriverEjecucionComando d){
		try {			
			objectMessage.setObject(parceTrama(d.getTramaBccs()));
			queueSender.send(objectMessage);
			
			log.info("Se envio correctamente: "+d.getCuenta()+":"+d.getTramaBccs());
		} catch (JMSException ex) {
			// TODO: handle exception
			log.error("ERROR sendCuenta:", ex);
		}
	}
	
	public static Cuenta parceTrama(String trama) {
		Cuenta cuenta = new Cuenta();
		String separador = (String) P.getParamVal(ParametroID.JMS_SEPARADOR_TRAMA);
		String[] ss = trama.trim().split(separador);		
		String imsi = ss[0];
		String proceso = ss[3];
		String fechaHora = ss[4];
		String otros = ss[9];
		cuenta.setNroCuentaA(imsi);
		cuenta.setProceso(proceso);
		cuenta.setOtros(otros);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Calendar cal=Calendar.getInstance();
		try {
			cal.setTime(dateFormat.parse(fechaHora));
		} catch (ParseException e) {
			log.error("ERROR : asignar fecha cuenta:"+fechaHora);
			e.printStackTrace();
		}
		cuenta.setFechaHora(cal);		
		return cuenta;
	}
	
	public List<DriverEjecucionComando> getListadriverejecucion() {
		return listadriverejecucion;
	}
	public String getCuentaenviadas() {
		return cuentaenviadas;
	}
	
	public void close(){
		try {
			queueConnection.close();
			log.info("Se Cerro exitosamente la conexion");
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			log.error("ERROR no se logro cerrar la conexion");
			e.printStackTrace();
		}
	}
	
	public void abrirconexion() throws NamingException, JMSException{
		InitialContext ic = getInitialContext();
		init(ic, (String) P.getParamVal(ParametroID.NAME_QUEUE_TOPIC));
		log.info("Se abrio exitosamente la conexion");
	}
}
