package micrium.driver.bussines;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import micrium.driver.dao.DriverRangoTelefoniaDao;
import micrium.driver.model.DriverRangoTelefonia;

public class DriverRangoTelefoniaBL implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private DriverRangoTelefoniaDao dao;
	
	public boolean Insertar(DriverRangoTelefonia driverrangotelefonia){
		return dao.Insertar(driverrangotelefonia);
	}
	public boolean Modificar(DriverRangoTelefonia driverrangotelefonia){
		return dao.Modificar(driverrangotelefonia);
	}
	public List<DriverRangoTelefonia> listar(){
		return dao.Listar();
	}
}
