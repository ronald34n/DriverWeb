package micrium.driver.bussines;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import bo.com.micrium.callgate.ws.MediationSipTrunk;
import bo.com.micrium.callgate.ws.ResponseType;
import micrium.driver.dao.CallGateDAO;

@Named
public class CallGateBL  implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(CallGateBL.class);
	@Inject
	private CallGateDAO callGateDAO;
	
	public String message ="";
	
	public ResponseType saveMediationSipTrunk(MediationSipTrunk mediationSipTrunk) throws Exception {
		return callGateDAO.saveMediationSipTrunk(mediationSipTrunk);		
	}
	
	public ResponseType updateMediationSipTrunk(MediationSipTrunk mediationSipTrunk) throws Exception {
		return callGateDAO.updateMediationSipTrunk(mediationSipTrunk);		
	}

	public ResponseType disableMediationSipTrunk(long id, String detail) throws Exception {
		return callGateDAO.disableMediationSipTrunk(id, detail);
	}

	public List<MediationSipTrunk> getAll() throws Exception {
		return callGateDAO.getAll();
	}
	
	public MediationSipTrunk getMediationSipTrunk(long id) throws Exception
	{
		return callGateDAO.getMediationSipTrunk(id);
	}
	
	
	
}
