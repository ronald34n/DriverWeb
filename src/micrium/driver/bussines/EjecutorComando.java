package micrium.driver.bussines;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.tigo.utils.SysMessage;

import micrium.driver.dao.DriverRangoTelefoniaDao;
import micrium.driver.model.DriverEjecucionComando;
import micrium.eventos_bccs.modelo.Cuenta;
import micrium.telnet.ClienteMsc;
import micrium.telnet.ClienteSSL;
import micrium.user.id.ParametroID;
import micrium.user.sys.P;

public class EjecutorComando {
	
	private ClienteMsc clienteTelnet;
	
	private final String LST_CLRDSG="LST CLRDSG: DSP=200;"; //LST CLRDSG: DSP=200;
	private final String LST_CLDPREANA="LST CLDPREANA: PFX=K'%CUENTA%, QR=LOCAL"; //LST CLDPREANA: PFX=K' 26350001, QR=LOCAL;  
	
	
	@Inject
	private DriverRangoTelefoniaDao rangotelefoniadao;
	
	public static Logger log = Logger.getLogger(EjecutorComando.class);
	
	public EjecutorComando() {
		// TODO Auto-generated constructor stub
	}
	
	public void init() throws Exception{
		clienteTelnet = new ClienteSSL();
		rangotelefoniadao=new DriverRangoTelefoniaDao();
	}
	
	public Map<String,String> ConsultarEstado(String cuenta){
		boolean mscHabilitado = (boolean) P.getParamVal(ParametroID.MSC_HABILITADO);
		if (!mscHabilitado){
			Map<String,String>mensaje=new HashMap<String,String>();
			//mensaje.put("LST_CLRDSG", "La cuenta si esta en CLRDSG (desbloqueado la llamada saliente)");
			//mensaje.put("LST_CLDPREANA","la cuenta Si esta CLDPREANA (Bloqueado la llamada entrante)");
			mensaje.put("LST_CLRDSG", "La cuenta esta desbloqueado la llamada saliente");
			mensaje.put("LST_CLDPREANA","la cuenta esta Bloqueado la llamada entrante)");
			mensaje.put("CUENTA",cuenta);
			return mensaje;
		}else{
		
		Map<String,String>mensajes=new HashMap<String, String>();
		mensajes.put("CUENTA",cuenta);
		//primero validamos el rango 
		//Cuenta cuenta=parcecuenta(driverejecucioncomando.getTramaBccs());
		//int numero = Integer.valueOf(cuenta.getIMSI());
        String plan = validarRango(Integer.parseInt(cuenta));
        log.info("Plan: " + plan);
		String comando1=LST_CLDPREANA.replace("%CUENTA%",cuenta);
        //una vez validado el rango empezamos a hacer la conexion y la ejecucion de los comandos para verificar el estado de la cuenta
		try {
			String pront = clienteTelnet.login();
			log.info("logueado correctamente MSC :"+pront);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("No logro loguarse correctamente cliente telnet");
			SysMessage.error("Fallo consultar estao cuenta, No pudo realizar conexion MSC");
			return null;
		}
				
		 try {
			String pront = clienteTelnet.sendComandInit();
			log.info("Exitoso al iniciar primer comando :"+pront);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("ERROR inicar primer comando");
		}
		 
        try {
        	//primer vamos a consultar estado cuenta CLDPREANA
        	log.info("Comando a ejecutar consulta CLDPREANA :"+comando1);
			String result1=clienteTelnet.sendComando(comando1);
			log.info("resultado de la ejecucion :"+result1);
			if (result1.indexOf("RETCODE = 0")!=-1){
				//encontro entonces
				//mensajes.add("la cuenta Si esta CLDPREANA (Bloqueado la llamada entrante)");
				//mensajes.put("LST_CLDPREANA","la cuenta Si esta CLDPREANA (Bloqueado la llamada entrante)");
				mensajes.put("LST_CLDPREANA","la cuenta esta Bloqueado la llamada entrante");
			}else{
				//mensajes.put("LST_CLDPREANA", "la cuenta no esta en  CLDPREANA(desbloqueado la llamada entrante)");
				mensajes.put("LST_CLDPREANA", "la cuenta esta bloqueado la llamada entrante");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("ERROR consultar CLDPREANA :"+e);
		}
        
        //ahora pasamos a la segunda ejecucion del comando CLRDSG
        try{
        	log.info("Comando a ejecutar consulta CLRDSG :"+LST_CLRDSG);
        	String result2=clienteTelnet.sendComando(LST_CLRDSG);
        	log.info("resultado de la ejecucion :"+result2);
        	if (result2.indexOf(cuenta)!=1){
        		//encontro entonces muestra mensaje
        		//mensajes.put("LST_CLRDSG","La cuenta si esta en CLRDSG (desbloqueado la llamada saliente)");
        		mensajes.put("LST_CLRDSG","La cuenta esta desbloqueado la llamada saliente");
        	}else{
        		//mensajes.put("LST_CLRDSG","la cuenta no esta en CLRDSG(bloqueado la llamada saliente)");
        		mensajes.put("LST_CLRDSG","la cuenta esta bloqueado la llamada saliente");
        	}
        	clienteTelnet.close();
        }catch (Exception e) {
        	log.error("ERROR consultar LST_CLRDSG"+e);
		}
        return mensajes;}
        
	}
	
	private Cuenta parcecuenta(String trama){
		String separador = (String) P.getParamVal(ParametroID.JMS_SEPARADOR_TRAMA);
		String[] ss = trama.trim().split(separador);
		String imsi = ss[0];
		String proceso = ss[3];
		String otros = ss[9];

		Cuenta cuenta=new Cuenta();
		cuenta.setIMSI(imsi);
		cuenta.setProceso(proceso);
		cuenta.setOtros(otros);
		return cuenta;
	}
	
	private String validarRango(int numero) {

        try {
            //return this.rangoTelefoniaDao.obtenerPlanComercial(numero);
        	return rangotelefoniadao.obtenerPlanComercial(numero);
        } catch (Exception ex) {
            log.error("No se logro validar el rango para obtener el plan comercia del numero: " + numero, ex);
        }
        return "No logro validar el rango";
    }
	
	/**
	 * Comvierte Una linea leida del servidor MSC a columnas adecuadas
	 *
	 * @param string Linea leida del servidor
	 * @return Una lista con los campos parceados
	 */
	private List<String> parce(String string) {
		List<String> list = new ArrayList<>();
		StringBuilder palabra = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) != ' ') {
				palabra.append(string.charAt(i));
			} else {
				if (i + 1 < string.length() && string.charAt(i + 1) != ' ') {
					palabra.append(string.charAt(i));
				} else {
					if (palabra.length() > 0) {
						list.add(palabra.toString().trim());
						palabra = new StringBuilder();
					}
				}

			}
		}

		return list;
	}
	
	
}
