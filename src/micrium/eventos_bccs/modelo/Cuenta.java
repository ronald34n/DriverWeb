package micrium.eventos_bccs.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import micrium.user.id.ParametroID;
import micrium.user.sys.P;

import org.apache.log4j.Logger;

public class Cuenta implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(Cuenta.class);

	private String nroCuentaA;
	private String IMSI;
	private String nroCuentaB;
	private String proceso;
	private Calendar fechaHora;
	private String otros;
	private String billetera;
	private BigDecimal monto;
	private Integer vigencia;
	private String comentario;

	public String getNroCuentaA() {
		return nroCuentaA;
	}

	public void setNroCuentaA(String nroCuentaA) {
		this.nroCuentaA = nroCuentaA;
	}

	public String getIMSI() {
		return IMSI;
	}

	public void setIMSI(String iMSI) {
		IMSI = iMSI;
	}

	public String getNroCuentaB() {
		return nroCuentaB;
	}

	public void setNroCuentaB(String nroCuentaB) {
		this.nroCuentaB = nroCuentaB;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public Calendar getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Calendar fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getOtros() {
		return otros;
	}

	public void setOtros(String otros) {
		this.otros = otros;
	}

	public String getBilletera() {
		return billetera;
	}

	public void setBilletera(String billetera) {
		this.billetera = billetera;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Integer getVigencia() {
		return vigencia;
	}

	public void setVigencia(Integer vigencia) {
		this.vigencia = vigencia;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String toTrama() {
		String pipe = "|";
		String fechaHoraString;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			fechaHoraString = sdf.format(fechaHora.getTime());
		} catch (Exception e) {
			log.error("[cuentaA: " + nroCuentaA + ", cuentaB: " + nroCuentaB + "] [Fallo al dar formato a la fecha]", e);
			fechaHoraString = "";
		}
		return nroCuentaA + pipe + nroCuentaB + pipe + IMSI + pipe + proceso + pipe + fechaHoraString + pipe + billetera + pipe + monto + pipe + vigencia + pipe + comentario + pipe + otros;
	}

	public String toString() {
		return "nroCuentaA :" + nroCuentaA + "\tIMSI :" + IMSI + "\tnroCuentaB :" + nroCuentaB + "\tproceso :" + proceso + "\tfechaHora :" + fechaHora + "\totros :" + otros + "\tbilletera :" + billetera + "\tmonto :" + monto + "\tvigencia :"
				+ vigencia + "\tcomentario :" + comentario;
	}
	
	
	public Cuenta convertir(String trama){
		Cuenta cuenta = new Cuenta();
		String separador = (String) P.getParamVal(ParametroID.JMS_SEPARADOR_TRAMA);
		String[] ss = trama.trim().split(separador);		
		String imsi = ss[2];
		String proceso = ss[3];
		String fechaHora = ss[4];
		String otros = ss[9];
		cuenta.setIMSI(imsi);
		cuenta.setProceso(proceso);
		cuenta.setOtros(otros);
		Calendar c=Calendar.getInstance();
		SimpleDateFormat df=new SimpleDateFormat(" dd-MM-yyyy HH:mm:ss");
		try {
			c.setTime(df.parse(fechaHora));
		} catch (ParseException e) {
			log.error("ERROR formato fecha cuenta:"+fechaHora);
			e.printStackTrace();
		}
		cuenta.setFechaHora(c);			
		return cuenta;
	}
}
